# assignment

## Objective 1

### 1. How would you access a \*NIX server remotely in order to debug a problem?

By logging in via SSH.

### 2. How would you version your application?

I would use SemVer, [despite being a flawed concept](https://gist.github.com/jashkenas/cbd2b088e20279ae2c8e). It's the de-facto standard.

### 3. How do you deliver your application to your team and for deployment?

As a Docker image.

### 4. If you would have to implement an authorisation / authentication system, what kind of patterns you would choose?

Lately I've been using OIDC & OAuth even for 1st party scenarios (skipping the user consent part in that specific scenario). The underlying solution depends really on what the needs are. At the simplest, I would always assign a role to the user (there's always gonna be at least 2 roles, User and Admin) and just check permissions in the code. Whenever it makes sense, it would be better to not have these evaluations hardcoded in the logic but rather implement a more elaborated RBAC solution, either in-house or using an OSS solution such as ORY Keto.

## Objective 2

### 5. We need to implement a Book store REST API using Spring Framework for Company XYZ.

I couldn't really do this without spending too much time: not only because I never used Spring Framework, but also it's been quite a while since I worked with Java at all.

Let me anyway say briefly how I would do it:

In general I would apply the following:

-   12factor design
-   Write an OpenAPI spec
-   Validate API inputs using the spec (JSON Schema)
-   Write integration tests for every API endpoint
-   Have an uniform JSON structure for all error responses
-   Expose health and ready endpoints for probes
-   Log unhandled errors
-   Make the API instance become "unhealthy" upon unhandled error, so it stops receiving traffic and replaced by a new instance -- assuming we deploy with HA

#### 5.1. Frontend should be able to display a list of books which contain the Name and Author's name

The API should expose a `GET /books` endpoint, returning status `200` and an `application/json` body with an array of books.

#### 5.2. Book has fields such as: ID, ISBN, Name, Author, Categories

I would create a `book` table (I personally like Postgres -- could also be done in a "NoSQL" database such as Mongo or DynamoDB).

#### 5.3. Frontend should be able to show, all the details of each book.

Then I would implement another API endpoint, `GET /books/:id` that returns status `200` and JSON body with the given book data, or status `404` and a JSON error if there is no book with the given id.

#### 5.4. An admin in the XYZ company should be able to add books to the store.

I guess we assume the account creation and authentication APIs are also implemented (they shouldn't be part of this API).

I would create another endpoint, `POST /books` that expects a token (could be a JWT) in the `Authorization` header. The API logic for this should:

1. validate the token (e.g. check the JWT signature, expiration and other claims)
2. ensure the bearer user has the proper role/rights (e.g. user X has "admin" role)
3. validate the request input (e.g. JSON body with the book details)
4. insert the book into the database

The API would return status `201`, a `Location` header with the URL for the given book, e.g. `/books/097c6ad1-e30a-4db8-b71a-b529ba2257bd` and a JSON body with the id as well.

#### 5.5. The XYZ company should have the ability to know when the book was inserted and updated in the system.

Despite many database have the option to timestamp this automatically, I prefer to do it in the business logic. So the `POST` and `PUT` endpoints for adding/updating a book would add this timestamp when writing to the database.

#### 5.6. The API should be able to handle Unexpected scenarios and return to the clients.

Not sure what you mean here. If talking about unexpected-but-kind-of-expected scenarios such as validation/authorization/data not found errors, then the API should return the proper status code along with the JSON error body explaining what happened.

For a literally unexpected error, e.g. database is down, out of memory, etc. the API should return a `500` response and set itself in "unhealthy" mode (i.e. its health endpoint starts responding with a `503` code) and just wait to be replaced by the orchestrator (k8s, ECS, a custom agent, etc).

## Objective 3

### 6. Frontend should be able to show the last access time of a book (Extension of requirement no 5.3)

Example:
Client A accessed book number 1 at T1. Client B accessed book number 1 at T2.
If frontend wants to show information about a book then the REST API response should contain last access time as T2 where (T1 < T2).

#### 6.1. Please explain at least 2 different approaches and their pros and cons to solve the above problem.

### Approach 1: just updating this timestamp in the database with every access.

`GET /books/:id` would perform 2 queries, a read (to get the book details) and an update (to put current access timestamp). Or, if the db allows it, perform an update operation that also returns the **old** data.

Additionally, instead of constantly updating the same table, we could have a `book_access` table with `book_id`, `access_time` columns and a composite index on both. With every access, a new row is appended. Then we perform an additional query for the last (max) access time for the given book id. Depending on the db engine this can certainly be an improvement, but we'd still have the cons below. Plus, we now need to do housekeeping on this table or either have a table for each period (month, year, etc).

You can also dispatch the update/insert query asynchronously and not wait for the update.

Pros:

-   simple solution, easy to understand, easy to refactor
    Cons:
-   now we are doing tons more of writes
-   and potentially executing an additional db query for every request
-   so more workload to the database
-   doesn't scale

### Approach 2: cache

Since books rarely change, we can cache their data and always read from the cache, unless there's a miss -- then we go to the database. Because books don't change, we can have them in the cache with a very high TTL, and if volume/RAM is a concern, we can configure an LRU/LFU eviction policy.

So there will be far less reads in our database, but we still need to write the access timestamp every time (now in both the cache and the database).

Pros:

-   less workload in the database
-   leverage the cache that very probably we would use anyway
    Cons:
-   more operations, slightly more complex code

#### 6.2. If you use any technology or architecture to solve this problem, please explain the pros and cons for doing so.

Redis.

#### 6.3. Choose one approach from 6.1. And explain why you have chosen this solution and the potential problems.

I would go with approach #2 because I think is better and the effort is not really much bigger.
